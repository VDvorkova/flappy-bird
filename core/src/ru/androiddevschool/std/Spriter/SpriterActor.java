package ru.androiddevschool.std.Spriter;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.brashmonkey.spriter.Data;
import com.brashmonkey.spriter.Player;
import com.brashmonkey.spriter.SCMLReader;

/**
 * Created by Гриша on 15.05.2017.
 */
public class SpriterActor extends Actor {
    private Player animation;
    private Drawer drawer;
    /**
     * @param handle SCML-file
     * @param atlas atlas
     */
    public SpriterActor(FileHandle handle, TextureAtlas atlas){
        Data data = new SCMLReader(handle.read()).getData();
        Loader loader = new Loader(data, atlas);
        loader.load(handle.file());
        drawer = new Drawer(loader);
        animation = new Player(data.getEntity(0));
    }

    /** Called when the actor's position has been changed. */
    protected void positionChanged () {
        animation.setPosition(getX(), getY());
    }

    //TODO: сделать масштабирование, альфаканал, точку поворота
    /** Called when the actor's size has been changed. */
    protected void sizeChanged () {
    }

    /** Called when the actor's rotation has been changed. */
    protected void rotationChanged () {
        animation.setAngle(getRotation());
    }

    public void act(float delta){
        super.act(delta);
        animation.update();
        animation.setPosition(getX(), getY());
    }

    public void draw(Batch batch, float parentAlpha){
        super.draw(batch,parentAlpha);
        drawer.draw(animation);
    }

    public Player getAnimation() {
        return animation;
    }
}
