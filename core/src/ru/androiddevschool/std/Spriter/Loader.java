package ru.androiddevschool.std.Spriter;

/**
 * Created by Гриша on 15.05.2017.
 */

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;
import com.brashmonkey.spriter.Data;
import com.brashmonkey.spriter.FileReference;

public class Loader extends com.brashmonkey.spriter.Loader<Sprite> {
    private TextureAtlas atlas;
    public Loader(Data data, TextureAtlas atlas) {
        super(data);
        String indexPrefix = "_";
        this.atlas = atlas;

        Array array = this.atlas.getRegions();

        for(int i = 0; i < array.size; ++i) {
            AtlasRegion region = (AtlasRegion)array.get(i);
            if(region.index != -1) {
                region.name = region.name + indexPrefix + region.index;
            }
        }

    }

    protected Sprite loadResource(FileReference ref) {
        return this.atlas.createSprite(this.data.getFile(ref).name.replace(".png", ""));
    }

    public void dispose() {
        this.atlas.dispose();
    }
}
