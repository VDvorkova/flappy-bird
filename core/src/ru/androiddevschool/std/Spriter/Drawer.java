package ru.androiddevschool.std.Spriter;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.brashmonkey.spriter.Timeline.Key.Object;

import static ru.androiddevschool.std.Utils.Global.batch;
import static ru.androiddevschool.std.Utils.Global.renderer;

/**
 * Created by Гриша on 15.05.2017.
 */

public class Drawer extends com.brashmonkey.spriter.Drawer<Sprite> {
    private Sprite sprite;

    public Drawer(Loader loader) {
        super(loader);
    }

    public void setColor(float r, float g, float b, float a) {
        renderer.setColor(r, g, b, a);
    }

    public void rectangle(float x, float y, float width, float height) {
        renderer.rect(x, y, width, height);
    }

    public void line(float x1, float y1, float x2, float y2) {
        renderer.line(x1, y1, x2, y2);
    }

    public void circle(float x, float y, float radius) {
        renderer.circle(x, y, radius);
    }

    public void draw(Object object) {
        sprite = loader.get(object.ref);
        float newPivotX = sprite.getWidth() * object.pivot.x;
        float newX = object.position.x - newPivotX;
        float newPivotY = sprite.getHeight() * object.pivot.y;
        float newY = object.position.y - newPivotY;
        sprite.setX(newX);
        sprite.setY(newY);
        sprite.setOrigin(newPivotX, newPivotY);
        sprite.setRotation(object.angle);
        sprite.setColor(1.0F, 1.0F, 1.0F, object.alpha);
        sprite.setScale(object.scale.x, object.scale.y);
        sprite.draw(batch);
    }
}