package ru.androiddevschool.std.Utils;

import com.badlogic.gdx.Gdx;

import java.util.Random;

/**
 * Created by Гриша on 04.05.2017.
 */
public class Values {
    public static final float WORLD_WIDTH = 540;
    public static final float WORLD_HEIGHT = 960;
    public static float PPU_X;
    public static float PPU_Y;
    public static Random r = new Random();//Нужно и в трубе и в птичке
    //Game constants
    public static final float birdHorizontalVelocity = 130;
    public static final float birdVerticalVelocity = 550;
    public static final float g = -2000;
    public static final float frequency = 10;
    public static final float amplitude = 3;
}
