package ru.androiddevschool.flappybird.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;

import ru.androiddevschool.flappybird.FlappyBird;
import ru.androiddevschool.flappybird.Model.Bird;
import ru.androiddevschool.flappybird.Model.Ground;
import ru.androiddevschool.flappybird.Model.World;
import ru.androiddevschool.flappybird.Utils.Assets;
import ru.androiddevschool.flappybird.Utils.Values;
import ru.androiddevschool.std.Screens.StdScreen;
import ru.androiddevschool.std.Utils.Global;

/**
 * Created by 03k1102 on 06.05.2017.
 */
public class Play extends StdScreen {
    private Bird bird;
    private Ground ground;
    private Image bg;
    private Label score;
    private int currentScore;
    private World world;

    public Play() {
        super();
    }

    @Override
    protected void initBg(Stage stage) {

    }

    @Override
    protected void initWorld(Stage stage) {
        world = new World(stage.getViewport(), stage.getBatch(), this);
        world.addActor(new Bird(Assets.get().images.get("small-animal2"), 50, Values.WORLD_HEIGHT/2));
        world.addActor(new Ground(Assets.get().images.get("ground").getRegion(), 0, 0));
        setStage(StageType.WORLD, world);

    }

    @Override
    protected void initUi(Stage stage) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/snap.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 60;
        parameter.magFilter = Texture.TextureFilter.Linear;
        parameter.minFilter = Texture.TextureFilter.Linear;

        currentScore = 0;
        score = new Label("0", new Label.LabelStyle(generator.generateFont(parameter), new Color(0x507299FF)));
        score.setAlignment(Align.center);
        score.setPosition((Values.WORLD_WIDTH - score.getWidth()) / 2, 745);
        stage.addActor(score);
    }

    public void show() {
        super.show();
        currentScore = 0;
        world.reset();
        Gdx.input.setInputProcessor(world);
    }

    public void render(float delta) {
        super.render(delta);
        currentScore += world.getPoints();
        score.setText("" + currentScore);
        score.setX((Values.WORLD_WIDTH - score.getWidth()) / 2);
    }

    public void lose() {
        Global.game.setScreen(ScreenName.MENU);
    }
}