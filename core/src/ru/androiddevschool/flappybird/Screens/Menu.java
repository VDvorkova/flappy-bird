package ru.androiddevschool.flappybird.Screens;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;

import ru.androiddevschool.flappybird.Utils.Assets;
import ru.androiddevschool.flappybird.Utils.Values;
import ru.androiddevschool.std.Controller.ScreenTraveler;
import ru.androiddevschool.std.Screens.StdScreen;

import static ru.androiddevschool.std.Utils.Names.ScreenName.*;

/**
 * Created by 03k1102 on 04.02.2017.
 */
public class Menu extends StdScreen {

    public Menu() {
        super();
    }


    @Override
    protected void initBg(Stage stage) {
        Image image;
        image = new Image(Assets.get().images.get("ground"));
        stage.addActor(image);
        image = new Image(Assets.get().images.get("clouds"));
        image.setPosition(0, Values.WORLD_HEIGHT, Align.top);
        stage.addActor(image);

    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {
        Button button;
        Table table = new Table();
        table.setFillParent(true);

        button = new TextButton("Play", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("menu"));
        button.addListener(new ScreenTraveler(LEVELS));
        table.add(button).pad(10).row();

        button = new TextButton("Settings", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("menu"));
        button.addListener(new ScreenTraveler(SETTINGS));
        table.add(button).pad(10).row();

        stage.addActor(table);

    }
}
