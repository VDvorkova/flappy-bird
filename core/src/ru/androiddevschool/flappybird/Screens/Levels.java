package ru.androiddevschool.flappybird.Screens;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;

import ru.androiddevschool.flappybird.Utils.Assets;
import ru.androiddevschool.flappybird.Utils.Values;
import ru.androiddevschool.std.Controller.ScreenTraveler;
import ru.androiddevschool.std.Screens.StdScreen;

import static ru.androiddevschool.std.Utils.Names.ScreenName.*;

/**
 * Created by 03k1102 on 08.04.2017.
 */
public class Levels extends StdScreen {
    public Levels() {
        super();
    }

    @Override
    protected void initBg(Stage stage) {
        Image image;
        image = new Image(Assets.get().images.get("ground"));
        stage.addActor(image);
        image = new Image(Assets.get().images.get("clouds"));
        image.setPosition(0, Values.WORLD_HEIGHT, Align.top);
        stage.addActor(image);
    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {
        Button button;
        Table table = new Table();
        table.setFillParent(true);

        table.add().expandX();
        table.add().expandX();
        table.row();

        button = new Button(Assets.get().buttonStyles.get("homebtn"));
        button.addListener(new ScreenTraveler(MENU));
        table.add(button).left().top();
        table.add().expandX().row();
        table.add().colspan(2).expand().row();

        button = new Button(Assets.get().buttonStyles.get("basket"));
        button.addListener(new ScreenTraveler(SHOP));
        table.add(button).padBottom(100).padRight(20).right();


        button = new Button(Assets.get().buttonStyles.get("play"));
        button.addListener(new ScreenTraveler(PLAY));
        table.add(button).padBottom(100).padLeft(20).left();


        stage.addActor(table);


        table = new Table();
        table.setFillParent(true);


        button = new TextButton("1", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("level"));
        table.add(button).pad(50).size(100);
        button = new TextButton("2", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("level"));
        table.add(button).pad(50).size(100);
        button = new TextButton("3", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("level"));
        table.add(button).pad(50).size(100).row();
        button = new TextButton("4", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("level"));
        table.add(button).pad(50).size(100);
        button = new TextButton("5", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("level"));
        table.add(button).pad(50).size(100);
        button = new TextButton("6", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("level"));
        table.add(button).pad(50).size(100).row();
        button = new TextButton("7", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("level"));
        table.add(button).pad(50).size(100);
        button = new TextButton("8", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("level"));
        table.add(button).pad(50).size(100);
        button = new TextButton("9", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("level"));
        table.add(button).pad(50).size(100).row();


        stage.addActor(table);

    }
}
