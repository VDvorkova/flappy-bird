package ru.androiddevschool.flappybird;

import com.badlogic.gdx.Gdx;

import ru.androiddevschool.flappybird.Screens.*;
import ru.androiddevschool.std.StdGame;

import static ru.androiddevschool.std.Utils.Names.ScreenName.*;

public class FlappyBird extends StdGame {
	private static FlappyBird instance = new FlappyBird();
	private FlappyBird() {super();}
	public static FlappyBird get() {
		return instance;
	}

	@Override
	public void postLoad() {
		Gdx.gl.glClearColor(1.0f*77/255, 1.0f*218/255, 1.0f*238/255, 1.0f);
		addScreen(MENU, new Menu());
		addScreen(PLAY, new Play());
		addScreen(SHOP, new Shop());
		addScreen(LEVELS, new Levels());
		setScreen(MENU);
	}
}
