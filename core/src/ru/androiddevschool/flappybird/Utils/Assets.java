package ru.androiddevschool.flappybird.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;

/**
 * Created by 03k1102 on 04.03.2017.
 */

public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initImages();
        initFonts();
        initStyles();
        //for (String name : images.keySet()) System.out.println(name);
    }

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();
        fonts.put("simple", new BitmapFont());

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/snap.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.color = new Color(0x1a15d2ff);
        parameter.size = 100;
        fonts.put("button", generator.generateFont(parameter));

        generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/cartwheel.otf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.color = new Color(0xffffffff);
        parameter.size = 100;
        parameter.borderColor = new Color(0xcd7f00ff);
        fonts.put("level", generator.generateFont(parameter));

    }

    private void initStyles() {
        buttonStyles = new HashMap<String, Button.ButtonStyle>();
        buttonStyles.put("menu", new TextButton.TextButtonStyle(images.get("button-up"), images.get("button-down"), null, fonts.get("button")));
        buttonStyles.put("level", new TextButton.TextButtonStyle(images.get("levelButton-up"), images.get("levelButton-down"), null, fonts.get("level")));
        buttonStyles.put("button play", new Button.ButtonStyle(images.get("knopka1"), images.get("knopka1"), null));
        buttonStyles.put("button help", new Button.ButtonStyle(images.get("knopka2"), images.get("knopka2"), null));
        buttonStyles.put("button shop", new Button.ButtonStyle(images.get("knopka3"), images.get("knopka3"), null));
        buttonStyles.put("button level", new Button.ButtonStyle(images.get("knopka4"), images.get("knopka4"), null));
        buttonStyles.put("homebtn", new Button.ButtonStyle(images.get("home 2 down"), images.get("home 2 up"), null));
        buttonStyles.put("basket", new Button.ButtonStyle(images.get("basket"), images.get("basketup"), null));
        buttonStyles.put("play", new Button.ButtonStyle(images.get("play-up"), images.get("play-down"), null));
    }


    private void initImages() {
        images = new HashMap<String, TextureRegionDrawable>();
        addImagesFolder("/ui");
        addImagesFolder("/imgs");
        addImagesFolder("/Square");
        for(String name : images.keySet())
            System.out.println(name);

    }

    public void addImagesFolder(String folderName) {
        FileHandle file = Gdx.files.local(folderName);
        for (FileHandle f : file.list()) {
            if (f.extension().equals("png")) {
                images.put(f.nameWithoutExtension(), getDrawable(f));
            } else {
                System.out.println("not valid asset extention");
            }
        }
    }

    public TextureRegionDrawable getDrawable(FileHandle file) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(file)));
    }

    public HashMap<String, TextureRegionDrawable> images;
    public HashMap<String, Button.ButtonStyle> buttonStyles;
    public HashMap<String, Label.LabelStyle> labelStyles;
    public HashMap<String, BitmapFont> fonts;
}