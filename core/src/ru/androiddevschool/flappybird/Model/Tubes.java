package ru.androiddevschool.flappybird.Model;

import com.badlogic.gdx.scenes.scene2d.Group;

import java.util.ArrayList;

import ru.androiddevschool.flappybird.Utils.Assets;
import ru.androiddevschool.flappybird.Utils.Values;
import ru.androiddevschool.std.Screens.StdScreen;

/**
 * Created by 03k1102 on 06.05.2017.
 */
public class Tubes extends Group {
    private ArrayList<Tube> tubes;
    private static final float tubeSpace = 300;
    private static final float groundHeight = 375;
    private static final float tubesStart = 900;
    private Bird bird;
    private StdScreen screen;

    public Tubes(float x, StdScreen screen) {
        super();
        this.screen = screen;
        tubes = new ArrayList<Tube>();
        bird = null;
        setPosition(0, 0);
        for (int i = 0; i < 4; i++)
            tubes.add(new Tube(Assets.get().images.get("tube-top2"), Assets.get().images.get("tube-bottom2"), tubesStart + x + i * tubeSpace, 300 + groundHeight));
        for (Tube tube : tubes) addActor(tube);
    }

    public void setBird(Bird bird) {
        this.bird = bird;
        for (int i = 0; i < tubes.size(); i++)
            tubes.get(i).setPosition(tubesStart + bird.getX() + i * tubeSpace, Values.r.nextInt(400) + groundHeight);
    }

    public boolean overlaps() {
        for (Tube tube : tubes) if (tube.overlaps(bird)) return true;
        return false;
    }

    public int getPoints() {
        int res = 0;
        for (Tube tube : tubes)
            if (tube.passed && !tube.wait) {
                res++;
                //Assets.get().sounds.get("point").play();
                if (tubes.indexOf(tube) == 2) {
                    res++;
                    //screen.getAssets().sounds.get("hit").play(); TODO: сделать звук с двойным кликом
                }
                tube.wait = true;
            }
        return res;
    }

    public void act(float delta) {
        super.act(delta);
        for (Tube tube : tubes)
            if (bird.getX() - tube.getX() >= 2 * tubeSpace) {
                tube.setPosition(tube.getX() + tubes.size() * tubeSpace, Values.r.nextInt(400) + groundHeight);
            }
    }
}