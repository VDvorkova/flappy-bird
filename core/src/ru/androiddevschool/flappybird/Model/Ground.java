package ru.androiddevschool.flappybird.Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

import ru.androiddevschool.flappybird.Utils.Values;

/**
 * Created by 03k1102 on 06.05.2017.
 */
public class Ground extends Actor {
    private TextureRegion img;

    public Ground(TextureRegion img, float x, float y) {
        this.img = img;
        setName("ground");
        setPosition(x, y);
        setSize(img.getRegionWidth()*2,img.getRegionHeight());
    }

    public void act(float delta) {
        if (getStage().getCamera().position.x > getX() + img.getRegionWidth() + Values.WORLD_WIDTH)
            setX(getX() + img.getRegionWidth());
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY());
        batch.draw(img, getX() + img.getRegionWidth(), getY());
    }

    public boolean overlaps(Bird bird){
        return (getHeight() >= bird.getY());
    }
}
