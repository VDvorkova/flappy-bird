package ru.androiddevschool.flappybird.Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

import ru.androiddevschool.flappybird.Screens.Play;
import ru.androiddevschool.flappybird.Utils.Assets;

/**
 * Created by 03k1102 on 06.05.2017.
 */
public class World extends Stage {
    private Tubes tubes;
    private Bird bird;
    private Ground ground;
    private Play screen;

    public World(Viewport viewport, Batch batch, Play screen) {
        super(viewport, batch);
        this.screen = screen;
        tubes = new Tubes(600, screen);
        addActor(tubes);
    }

    public void act(float delta) {
        super.act(delta);
        if (tubes.overlaps() || ground.overlaps(bird)) {
            //Assets.get().sounds.get("hit").play();
            screen.lose();
        }
    }

    public int getPoints(){
        return tubes.getPoints();
    }

    public void addActor(Actor actor) {
        if (actor.getName() != null) {
            if (actor.getName().equals("ground")) ground = (Ground) actor;
            if (actor.getName().equals("bird")) {
                bird = (Bird) actor;
                tubes.setBird(bird);
            }
        }
        super.addActor(actor);
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        boolean res = super.touchDown(screenX, screenY, pointer, button);
        tap();
        return res;
    }

    public void tap(){
        //Assets.get().sounds.get("wing").play();
        bird.tap();
}

    public void reset() {
        bird.tap();
        tubes.setBird(bird);
    }

    public Bird getBird(){
        return bird;
    }
}