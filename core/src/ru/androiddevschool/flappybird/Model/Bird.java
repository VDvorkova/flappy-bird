package ru.androiddevschool.flappybird.Model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import ru.androiddevschool.flappybird.FlappyBird;
import ru.androiddevschool.flappybird.Utils.Assets;
import ru.androiddevschool.flappybird.Utils.Values;

/**
 * Created by 03k1102 on 06.05.2017.
 */
public class Bird extends Image {
    private Vector2 startPosition;
    private float moveDelta;
    private Vector2 velocity;

    public Bird(TextureRegionDrawable img, float x, float y) {
        super(img);
        setName("bird");
        setPosition(x, y);
        setAlign(Align.left);
        setOrigin(img.getRegion().getRegionWidth() / 2, img.getRegion().getRegionWidth() / 10);
        startPosition = new Vector2(x, y);
        moveDelta = 0;
        velocity = new Vector2(Values.birdHorizontalVelocity, 0);
    }

    public void act(float delta) {
        velocity.x = Values.birdHorizontalVelocity * 2;
        velocity.y += Values.g * delta;
        moveBy(velocity.x * delta, velocity.y * delta);
        getStage().getCamera().position.x = getX() + Values.WORLD_WIDTH/3;
    }


    public void tap() {
        velocity.y = Values.birdVerticalVelocity;
    }

    public void updateTexture() {
        TextureRegionDrawable img = Assets.get().images.get(String.format("animal%d", Values.r.nextInt(4)));
        setDrawable(img);
        setWidth(img.getRegion().getRegionWidth());
        setHeight(img.getRegion().getRegionHeight());
    }
}