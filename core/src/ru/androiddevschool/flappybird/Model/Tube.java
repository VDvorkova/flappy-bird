package ru.androiddevschool.flappybird.Model;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

/**
 * Created by 03k1102 on 06.05.2017.
 */
public class Tube extends Group {
    private Image imgTop;
    private Image imgBottom;
    private float space;
    public boolean passed;
    public boolean wait;

    public Tube(TextureRegionDrawable top, TextureRegionDrawable bottom, float x, float y, float space) {
        super();
        this.space = space;

        imgTop = new Image(top);
        imgBottom = new Image(bottom);

        addActor(imgTop);
        addActor(imgBottom);
        setPosition(x, y);
        setSize(imgTop.getWidth(), imgTop.getHeight() + imgBottom.getHeight() + 2 * space);
    }

    public Tube(TextureRegionDrawable top, TextureRegionDrawable bottom, float x, float y) {
        this(top, bottom, x, y, 90);
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        passed = false;
        wait = false;
        imgTop.setPosition(0, space);
        imgBottom.setPosition(0, -space - imgBottom.getHeight());
    }

    public boolean overlaps(Bird bird) {
        //System.out.printf("center : (%3f;%3f;%3f;%3f)\tbird : (%3f;%3f;%3f;%3f)\n", getX(), getY(), getWidth(), space, bird.getX(), bird.getY(), bird.getWidth(), bird.getHeight());
        if (bird.getX() > getX() + getWidth()) passed = true;
        return (
                (Math.abs(getY() - bird.getY() - bird.getHeight() / 2) > space - bird.getHeight() / 2) && (bird.getX() + bird.getWidth() >= getX()) && (bird.getX() <= getX() + getWidth())
        );
    }

}